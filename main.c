//
//  main.c
//
// File Test
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void ProcessFile(unsigned char *pData, unsigned long ulSize)
{
	printf("%d bytes read from file\n", (int)ulSize);
} /* ProcessFile() */

/****************************************************************************
 *                                                                          *
 *  FUNCTION   : main(int, char**)                                          *
 *                                                                          *
 ****************************************************************************/
int main( int argc, char *argv[ ])
{
FILE *in;
unsigned char *pBuffer;
unsigned long ulSize;

	if (argc < 2)
	{
		printf("Usage: filetest <filename>\n");
		return 0;
	}
	in = fopen(argv[1], "rb");
	if (in != NULL)
	{
	unsigned long ulStart;

		ulStart = ftell(in);
		fseek(in, 0L, SEEK_END);
		ulSize = ftell(in);
		fseek(in, ulStart, SEEK_SET);
		pBuffer = malloc(ulSize);
		fread(pBuffer, 1, ulSize, in);
		fclose(in);
		ProcessFile(pBuffer, ulSize);
	}

    return 0;
} /* main() */
