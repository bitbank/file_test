CFLAGS=-c -Wall -O2
LIBS = -lpthread

all: filetest

filetest: main.o
	$(CC) main.o $(LIBS) -o filetest

main.o: main.c
	$(CC) $(CFLAGS) main.c

clean:
	rm -rf *o filetest

